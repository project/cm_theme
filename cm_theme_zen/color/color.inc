<?php

/**
 * PART 1. Basic Color module settings
 */

$info = array();

// Define the possible replaceable items and their labels.
$info['fields'] = array(
  'base' => t('Page background color'),
  'line' => t('Line and active menu background color'),
  'menu-hover' => t('Menu link hover text color'),
  'light-link' => t('Menu link hover background'),
  'link' => t('Link color'),
  'hover' => t('Link hover color'),
  'active' => t('Active link color'),
  'bullet' => t('Bullets'),
  'grey' => t('Grey'),
  'header' => t('Header background'),
  'navigation' => t('Navigation background'),
  'branding' => t('Branding background'),
  'dark-background' => t('Footer background'),
  'light-text' => t('Footer link color'),
  'text' => t('Text color'),
);

// Color schemes for the site.
$info['schemes'] = array(
  // Define the default scheme.
  'default' => array(
    // Scheme title.
    'title' => t('Default colors'),
    // Scheme colors (Keys are coming from $info['fields']).
    'colors' => array(
	  'base' => '#cccccc',
	  'line' => '#ac2e32',
    'menu-hover' => '#fefefe',
	  'light-link' => '#e4aeae',
	  'link' => '#6d6e71',
	  'hover' => '#f2f3f4',
	  'active' => '#e6e7e8',
	  'bullet' => '#cc0001',
	  'grey' => '#4c4d4f',
    'header' => '#fdfdfd',
    'navigation' => '#fcfcfc',
    'branding' => '#fbfbfb',
    'dark-background' => '#312f30',
    'light-text' => '#bcbec0',
    'text' => '#000000',
	  ),
  ),
  // Let's create a scheme called OKV.
  'okv' => array(
    // Scheme title.
    'title' => t('OKV'),
    // Scheme colors (Keys are coming from $info['fields']).
    'colors' => array(
    'base' => '#cccccc',
    'line' => '#07527c',
    'menu-hover' => '#fefefe',
    'light-link' => '#e4aeae',
    'link' => '#6d6e71',
    'hover' => '#f2f3f4',
    'active' => '#e6e7e8',
    'bullet' => '#07527c',
    'grey' => '#4c4d4f',
    'header' => '#fdfdfd',
    'navigation' => '#fcfcfc',
    'branding' => '#fbfbfb',
    'dark-background' => '#312f30',
    'light-text' => '#bcbec0',
    'text' => '#000000',
    ),
  ),
  // Let's create a scheme called ROK.
  'rok' => array(
    // Scheme title.
    'title' => t('ROK'),
    // Scheme colors (Keys are coming from $info['fields']).
    'colors' => array(
    'base' => '#2980b9',
    'line' => '#3498db',
    'menu-hover' => '#fefefe',
    'light-link' => '#3498db',
    'link' => '#c0392b',
    'hover' => '#FF8000',
    'active' => '#ffffff',
    'bullet' => '#2980b9',
    'grey' => '#4c4d4f',
    'header' => '#fdfdfd',
    'navigation' => '#fcfcfc',
    'branding' => '#fbfbfb',
    'dark-background' => '#c0392b',
    'light-text' => '#ffffff',
    'text' => '#000000',
    ),
  ),
);

// Define the CSS file(s) that we want the Color module to use as a base.
$info['css'] = array(
  'css/colors.css',
);

/***** Advanced Color settings - Defaults. Will be used in the Part 2. *****/

/**
 * Default settings for the advanced stuff.
 * No need to edit these if you just want to play around with the colors.
 * Color wants these, otherwise it's not going to play.
 *
 * We dive deeper into these in the Part 2. Advanced Color settings
 */

// Files we want to copy along with the CSS files, let's define these later.
$info['copy'] = array(
  'logo.png'
);

// Files used in the scheme preview.
$info['preview_css'] = 'color/preview.css';
$info['preview_js'] = 'color/preview.js';
$info['preview_html'] = 'color/preview.html';

// Gradients
$info['gradients'] = array();

// Color areas to fill (x, y, width, height).
$info['fill'] = array();

// Coordinates of all the theme slices (x, y, width, height)
// with their filename as used in the stylesheet.
$info['slices'] = array();

// Base file for image generation.
$info['base_image'] = 'color/base.png';

// Reference color used for blending. Matches the base.png's colors.
$info['blend_target'] = '#ffffff';
