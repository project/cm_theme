// Handle the color changes and update the preview window.
(function ($) {
  Drupal.color = {
    logoChanged: false,
    callback: function(context, settings, form, farb, height, width) {
      // Background
      $('#preview', form).css('backgroundColor', $('#palette input[name="palette[base]"]', form).val());

      // Text
      $('#preview #main', form).css('color', $('#palette input[name="palette[text]"]', form).val());

      // Links
      $('#preview a', form).css('color', $('#palette input[name="palette[link]"]', form).val());

      // Header
      $('#preview #header', form).css('border-bottom-color', $('#palette input[name="palette[line]"]', form).val());

      // Navigation
      $('#preview #block-system-main-menu nav ul.menu li a.active', form).css('background-color', $('#palette input[name="palette[line]"]', form).val());
	    $('#preview #block-system-main-menu nav ul.menu li a.active', form).css('color', $('#palette input[name="palette[active]"]', form).val());
      $('#preview #header', form).css('background-color', $('#palette input[name="palette[header]"]', form).val());
      
      // Hover
      $('#preview #block-system-main-menu nav ul.menu li a').hover(function() {
        $(this).css('color', $('#palette input[name="palette[menu-hover]"]', form).val());
        $(this).css('background-color', $('#palette input[name="palette[light-link]"]', form).val());
      },
      function() {
        $(this).css('color', $('#palette input[name="palette[link]"]', form).val());
        $(this).css('background-color', $('#palette input[name="palette[header]"]', form).val());
      });

      // Footer
      $('#preview #footer', form).css('background-color', $('#palette input[name="palette[dark-background]"]', form).val());
      $('#preview #footer a', form).css('color', $('#palette input[name="palette[light-text]"]', form).val());
      $('#preview #footer', form).css('border-color', $('#palette input[name="palette[line]"]', form).val());
  
      // Bullets
      $('#preview-page #content ul li::before', form).css('color', $('#palette input[name="palette[bullet]"]', form).val());
  
    }
  };
})(jQuery);